export const daoSnippet = `import { ApiProperty } from '@nestjs/swagger';

export class {{class-name}} {

    {{dao-properties}}

}`;

export const apiPropertySnippet = `
    @ApiProperty({
        name: '{{property-name}}'
        description: '{{property-description}}',
    })
    readonly {{property-name}}: {{property-type}};
`;